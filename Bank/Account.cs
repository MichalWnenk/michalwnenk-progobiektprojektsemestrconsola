﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //    KLASA ABSTARCYJNA KONTO :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    [Serializable]
    abstract public class Account
    {
        // POLA KLASY KONTO :

        private String type; // typ konta
        private Int64 accountNumber; // numer konta
        private Double balance; // saldo
        private Double availableResources; // dostępne środki
        private List<Transfer> transferHistoryList; // historia przelewów

        // KONSTRUKTORY KLASY KONTO :

        public Account(String type, Int64 accountNumber, Double balance, Double availableResources, List<Transfer> transferHistoryList)
        {
            this.type = type;
            this.accountNumber = accountNumber;
            this.balance = balance;
            this.availableResources = availableResources;
            this.transferHistoryList = transferHistoryList;
        }

        // METODY ZWRACAJĄCE WARTOŚCI PÓL KLASY :

        public static String GetType(Account account)
        {
            return account.type;
        }

        public static Int64 GetAccountNumber(Account account)
        {
            return account.accountNumber;
        }

        public static Double GetBalance(Account account)
        {
            return account.balance;
        }

        public static Double GetAvailableResources(Account account)
        {
            return account.availableResources;
        }

        public static List<Transfer> GetTransferHistoryList(Account account)
        {
            return account.transferHistoryList;
        }

        // METODY ZMIENIAJĄCE WARTOŚCI PÓL "BALANCE" I "AVAILABLERESOURCES" :

        public static void SetBalance(Account account, Double amount)
        {
            account.balance = account.balance + amount;
        }

        public static void SetAvailableResources(Account account, Double amount)
        {
            account.availableResources = account.availableResources + amount;
        }

        // METODA LOSOWANIA LICZBY 64 BITOWEJ :

        public static Int64 NextInt64()
        {
            Random rnd = new Random();
            var buffer = new byte[sizeof(Int64)];
            rnd.NextBytes(buffer);
            Int64 a = BitConverter.ToInt64(buffer, 0);

            if (a < 0)
            {
                a = a * (-1);
            }

            return a;
        }

        // OTWIERANIE KONTO :

        public static Int64 OpenAccount(List<Int64> accountNumbersList)
        {
            Int64 a = NextInt64();
            while (accountNumbersList.Contains(a))
            {
                a = NextInt64();
            }
            accountNumbersList.Add(a);

            return a;
        }

        // METODA POKAŻ INFORMACJE O KONCIE :

        virtual public void ShowAccountInfo()
        {
            Console.WriteLine("Typ : {0}", type);
            Console.WriteLine("Numer Konta : {0}", accountNumber);
            Console.WriteLine("Saldo : {0} zł", balance);
            Console.WriteLine("Dostępne środki : {0} zł", availableResources);
        }

        // METODA SPRAWDZAJĄCA CZY ISNIEJE KONTO O PODANYM NUMERZE :

        public static Boolean CheckAccount(List<Account> accountsList, Int64 accountNumber)
        {
            Boolean bl = false;
            foreach (var item in accountsList)
            {
                if (item.accountNumber == accountNumber)
                {
                    bl = true;
                    break;
                }
            }
            return bl;
        }

        // METODA ZWRACAJĄCA POZYCJE KONTA NA LIŚCIE KONT :

        public static Int32 FindAccount(List<Account> accountsList, Int64 accountNumber)
        {
            int number = 0;
            foreach (var item in accountsList)
            {
                if (item.accountNumber == accountNumber)
                {
                    break;
                }
                number += 1;
            }
            return number;
        }

        public static void AddTransfer(User user, Transfer transfer, int number)
        {
            User.GetAccountsList(user)[number].transferHistoryList.Add(transfer);
        }

        // METODA SPRAWDZAJĄCA CZY PODANY CIĄG ZNAKÓW TO SŁOWO :

        public static Boolean CheckWord(string word)
        {
            Boolean bl = true;
            foreach (var letter in word)
            {
                while (bl == true)
                {
                    if (!(Char.IsLetter(letter) || letter == ' '))
                    {
                        Console.WriteLine();
                        Console.WriteLine("Niepoprawnie wporwadzono słowa, spróbuj jeszcze raz.");
                        Console.WriteLine();
                        bl = false;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return bl;
        }

        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
        //      KLASA KONTO NORMALNE :
        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

        [Serializable]
        public class NormalAccount : Account
        {
            // KONSTRUKTORY KLASY KONTO NORMALNEGO :

            public NormalAccount(String type, Int64 accountNumber, Double balance, Double availableResources, List<Transfer> transferHistoryList)
                : base(type, accountNumber, balance, availableResources, transferHistoryList)
            {
            }

        }

        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
        //       KLASA KONTO JUNIOR :
        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

        [Serializable]
        public class JuniorAccount : Account
        {
            // POLA KLASY KONTO JUNIOR :

            private String agent; // pełnomocnik

            // KONSTRUKTOR KLASY KONTO JUNIOR :

            public JuniorAccount(String type, Int64 accountNumber, Double balance, Double availableResources, String agent, List<Transfer> transferHistoryList)
                : base(type, accountNumber, balance, availableResources, transferHistoryList)
            {
                this.agent = agent;
            }

            // METODA USTAWIAJĄCA PEŁNOMOCNIKA :

            public static String SetAgent()
            {
                string agent;
                do
                {
                    Console.WriteLine("Podaj imię i nazwisko pełnomocnika :");
                    agent = Console.ReadLine();
                } while (!(CheckWord(agent)));

                return agent;
            }

            // NADPISANA METODA POKAŻ INFORMACJE :

            public override void ShowAccountInfo()
            {
                base.ShowAccountInfo();
                Console.WriteLine("Pełnomocnik : {0}", agent);
            }
        }

        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
        //       KLASA KONTO PROFIT :
        // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

        [Serializable]
        public class ProfitAccount : Account
        {
            // KONSTRUKTOR KLASY KONTO PROFIT :

            public ProfitAccount(String type, Int64 accountNumber, Double balance, Double availableResources, List<Transfer> transferHistoryList)
                : base(type, accountNumber, balance, availableResources, transferHistoryList)
            {
            }
        }
    }
}
