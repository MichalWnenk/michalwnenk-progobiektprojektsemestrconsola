﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //        KLASA UŻYTKOWNIK :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    [Serializable]
    public class User
    {
        // POLA KLASY UŻYTKOWNIK :

        private Int32 userNumber;
        private String password;
        private UserInfo userInfo;
        private List<Account> accountsList;

        // KONSTRUKTORY KLASY UŻYTKOWNIK :

        public User(Int32 userNumber, String password, UserInfo userInfo, List<Account> accountsList)
        {
            this.userNumber = userNumber;
            this.password = password;
            this.userInfo = userInfo;
            this.accountsList = accountsList;
        }

        // METODY ZWRACAJĄCE WARTOŚCI PÓL KLASY :

        public static Int32 GetUserNumber(User u)
        {
            return u.userNumber;
        }

        public static String GetPassword(User u)
        {
            return u.password;
        }

        public static UserInfo GetUserInfo(User u)
        {
            return u.userInfo;
        }

        public static List<Account> GetAccountsList(User u)
        {
            return u.accountsList;
        }

        // TWORZENIE NOWEGO KONTA UŻYTKOWNIKA :

        public User CreateUserLogin(User user, List<Int32> l)
        {
            Random a = new Random();
            int number = a.Next(10000000, 99999999);

            while (l.Contains(number))
            {
                number = a.Next(10000000, 99999999);
            }

            UserInfo userInfo = new UserInfo();
            UserInfo.Registration(userInfo);
            Console.WriteLine();

            Console.WriteLine("Wprowadź swoje hasło (max 8 znaków) :");
            String password1 = Console.ReadLine();
            Console.WriteLine("Wprowadź swoje hasło ponownie (max 8 znaków) :");
            String password2 = Console.ReadLine();

            while (password1 != password2)
            {
                Console.WriteLine();
                Console.WriteLine("Wprowadziłeś dwa różne hasła. Spróbuj ponownie :)");
                Console.WriteLine();
                Console.WriteLine("Wprowadź swoje hasło (max 8 znaków) :");
                password1 = Console.ReadLine();
                Console.WriteLine("Wprowadź swoje hasło ponownie (max 8 znaków) :");
                password2 = Console.ReadLine();
            }

            user.userNumber = number;
            user.password = password1;
            user.userInfo = userInfo;
            Console.WriteLine();
            Console.WriteLine("Twoje konto zostało utworzone pomyślnie!");
            Console.WriteLine();
            Console.WriteLine("Oto twój unikalny numer użytkownika :");
            Console.WriteLine(user.userNumber);
            Console.WriteLine();
            Console.WriteLine("Proszę go zapamiętać, będzie potrzabny przy każdym logowaniu :)");
            Console.WriteLine();

            return user;
        }

        // METODA SPRAWDZAJĄCA CZY ISNIEJE PODANY UŻYTKOWNIK :

        public static Boolean CheckUser(string name, string surname, long accountNumber, List<User> usersList)
        {
            Boolean bl = false;
            foreach (var user in usersList)
            {
                if (UserInfo.GetName(user.userInfo) == name && UserInfo.GetSurname(user.userInfo) == surname && Account.CheckAccount(user.accountsList, accountNumber))
                {
                    bl = true;
                    break;
                }
            }
            return bl;
        }

        // METODA ZWRACAJĄCA POZYCJE UŻYTKOWNIKA NA LIŚCIE KONT : 

        public static Int32 FindUser(string name, string surname, long accountNumber, List<User> usersList)
        {
            int number = 0;
            foreach (var user in usersList)
            {
                if (UserInfo.GetName(user.userInfo) == name && UserInfo.GetSurname(user.userInfo) == surname && Account.CheckAccount(user.accountsList, accountNumber))
                {
                    break;
                }
                number += 1;
            }
            return number;
        }
    }
}
