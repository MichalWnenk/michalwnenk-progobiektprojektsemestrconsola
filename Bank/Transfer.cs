﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //          KLASA TRANSFER :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    [Serializable]
    public class Transfer
    {
        // POLA KALSY BAZA DANYCH :

        private Int64 accountNumber;
        private String name;
        private String surname;
        private String adress;
        private String postCode;
        private Double amount;

        // KONTRUKTORY KLASY BAZA DANYCH :

        public Transfer()
        {
        }

        public Transfer(Int64 accountNumber, String name, String surname, String adress, String postCode, Double amount)
        {
            this.accountNumber = accountNumber;
            this.name = name;
            this.surname = surname;
            this.adress = adress;
            this.postCode = postCode;
            this.amount = amount;
        }

        // METODA POKAŻ INFORMACJE :

        public static void ShowTransferInfo(List<Transfer> l)
        {
            Console.WriteLine("Historia przelewów :");
            Console.WriteLine();
            int liczba = 1;
            foreach (var item in l)
            {
                Console.WriteLine("----------<{0}>----------", liczba);
                Console.WriteLine("[ {0} ]", item.accountNumber);
                Console.WriteLine("{0} {1}", item.name, item.surname);
                Console.WriteLine(item.postCode);
                Console.WriteLine("{0} zł", item.amount);
                Console.WriteLine("-----------------------");
                Console.WriteLine();
                liczba += 1;
            }
        }

        // METODA PRZELEWAJĄCA PIENIĄDZE :

        public static void MakeTransfer(User user, List<User> usersList)
        {
            if (User.GetAccountsList(user) != null)
            {
                Console.WriteLine("Z którego konta chcesz przelać pieniądze :");
                Console.WriteLine();
                int number = 1; // pomocnicza zmienna do wypisywania kont

                foreach (var item in User.GetAccountsList(user))
                {
                    Console.WriteLine("{0} -> {1}, [ {2} ]", number, Account.GetType(item), Account.GetAccountNumber(item));
                    Console.WriteLine();
                    number += 1;
                }
                Console.WriteLine("0 -> Powrót");

                int number1 = Convert.ToInt32(Console.ReadLine()); // numer konto wybrany przez użytkownika

                // SPRAWDZANIE CZY NIE WYBRANO OPCJI "POWRÓT" :

                if (number1 != 0)
                {
                    // SPRAWDZANIE CZY POPRAWNIE WYBRANO KONTO, Z KTOREGO BĘDĄ PRZELEWANE PIENIĄDZE : 

                    if (number1 > 0 && number1 < number)
                    {
                        Console.WriteLine("UZUPEŁNIJ DANE ODBIORCY :");
                        Console.WriteLine();

                        // SPRAWDZA CZY PODAŁĘŚ LICZBĘ :
                        long accountNumber = 0;
                        Boolean bl = true;
                        do
                        {
                            try
                            {
                                Console.WriteLine("Numer konto :");
                                accountNumber = Convert.ToInt64(Console.ReadLine());
                                Console.WriteLine();
                                bl = true;
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Nie wprowadziłeś poprawnie liczby!");
                                Console.WriteLine();
                                bl = false;
                            }

                        } while (!bl);

                        // SPRAWDZA CZY PODAŁEŚ SŁOWO :
                        string name;
                        string surname;
                        string adress;
                        string postCode;
                        do
                        {
                            if (bl == false)
                            {
                                Console.WriteLine("Nie wprowadziłeś poprawnie słowo lub słowa!");
                                Console.WriteLine();
                            }
                            Console.WriteLine("Imię :");
                            name = Console.ReadLine();
                            Console.WriteLine();
                            Console.WriteLine("Nazwisko :");
                            surname = Console.ReadLine();
                            Console.WriteLine();
                            Console.WriteLine("Adres (ulica, numer bloku, numer mieszkania) :");
                            adress = Console.ReadLine();
                            Console.WriteLine();
                            Console.WriteLine("Kod pocztowy (z miejscowością) :");
                            postCode = Console.ReadLine();
                            Console.WriteLine();
                            if (CheckWord(name) && CheckWord(surname) && CheckWord(adress) && CheckWord(postCode))
                            {
                                bl = true;
                            }
                            else
                            {
                                bl = false;
                            }
                        } while (!bl);

                        // SPRAWDZA CZY PODAŁĘŚ LICZBĘ :
                        double amount = 0;
                        do
                        {
                            try
                            {
                                Console.WriteLine("Kwota przelewu :");
                                amount = Convert.ToDouble(Console.ReadLine()); // kwota
                                Console.WriteLine();
                                bl = true;
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Nie wprowadziłeś poprawnie liczby!");
                                Console.WriteLine();
                                bl = false;
                            }

                        } while (!bl);

                        int number2 = 0; // zminna pomocnicza, pomagająca psrawdzic czy wciśnięto "Anuluj"

                        while (!(User.CheckUser(name, surname, accountNumber, usersList)) && number2 != 2)
                        {
                            Console.WriteLine("ŹLE WYPEŁNIONY FORMULARZ LUB NIEISTNIEJE TAKI UŻYTKOWNIK!");
                            Console.WriteLine();
                            Console.WriteLine("1 -> Spóbuj jeszcze raz");
                            Console.WriteLine("2 -> Anuluj");
                            number2 = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();

                            switch (number2)
                            {
                                case 1:
                                    Console.WriteLine("UZUPEŁNIJ DANE ODBIORCY :");
                                    Console.WriteLine();
                                    Console.WriteLine("Numer konto :");
                                    accountNumber = Convert.ToInt64(Console.ReadLine());
                                    Console.WriteLine();
                                    Console.WriteLine("Imię / Nazwa firmy :");
                                    name = Console.ReadLine();
                                    Console.WriteLine();
                                    Console.WriteLine("Nazwisko / Nazwa firmy :");
                                    surname = Console.ReadLine();
                                    Console.WriteLine();
                                    Console.WriteLine("Adres (ulica, numer bloku, numer mieszkania) :");
                                    adress = Console.ReadLine();
                                    Console.WriteLine();
                                    Console.WriteLine("Kod pocztowy (z miejscowością) :");
                                    postCode = Console.ReadLine();
                                    Console.WriteLine();
                                    Console.WriteLine("Kwota przelewu :");
                                    amount = Convert.ToDouble(Console.ReadLine()); // kwota
                                    break;

                                case 2:
                                    break;
                            }
                        }

                        if (number2 != 2)
                        {
                            // SPRAWDZANIE CZY WYKONAWCA MA ODPOWIEDNIO DUZO PIENIĘDZY :

                            if (Account.GetAvailableResources(User.GetAccountsList(user)[number1 - 1]) > amount)
                            {
                                // ODEJMUJEMY WYKONAWCY PRZELEWU :

                                Account.SetAvailableResources(User.GetAccountsList(user)[number1 - 1], -amount);
                                Account.SetBalance(User.GetAccountsList(user)[number1 - 1], -amount);

                                // DODAJEMY ODBIORYC PRZELEWU :

                                int ConsumerNumber = User.FindUser(name, surname, accountNumber, usersList); // zwraca numer użytkownika na liście użytkowników, do którego mają być przesłąne pieniądze
                                int ConsumerAccountNumber = Account.FindAccount(User.GetAccountsList(usersList[ConsumerNumber]), accountNumber); // zwraca numer konta na liście kont, do którego mają być przesłąne pieniądze

                                Account.SetAvailableResources(User.GetAccountsList(usersList[ConsumerNumber])[ConsumerAccountNumber], amount);
                                Account.SetBalance(User.GetAccountsList(usersList[ConsumerNumber])[ConsumerAccountNumber], amount);

                                Transfer transferMinus = new Transfer(accountNumber, name, surname, adress, postCode, -amount);
                                Transfer transferPlus = new Transfer(accountNumber, name, surname, adress, postCode, amount);

                                Account.AddTransfer(user, transferMinus, number1 - 1);
                                Account.AddTransfer(usersList[ConsumerNumber], transferPlus, ConsumerAccountNumber);

                            }
                            else // KOMUNIKAT JEŻELI PIENIĘDZY BRAK :
                            {
                                Console.WriteLine("Nie masz wystarczająco dużo środków na koncie aby wykonać tę operację!");
                            }
                        }
                    }
                    else // KOMUNIKAT JEŻELI ŹLE WYBRANO KONTO, Z KTÓREGO BĘDĄ PRZELEWANE PIENIĄDZE :
                    {
                        Console.WriteLine("Źle wybrałeś konto! Spróbuj jeszcze raz :");
                        Console.WriteLine();
                        MakeTransfer(user, usersList);
                    }
                }
            }
            else
            {
                Console.WriteLine("Nie można wykonoć przelewu, ponieważ nie utworzyłeś jeszcze ani jednego konto :)");
                Console.WriteLine();
            }
        }

        public static Boolean CheckWord(string word)
        {
            Boolean bl = true;
            foreach (var letter in word)
            {
                while (bl == true)
                {
                    if (!(Char.IsLetter(letter)))
                    {
                        Console.WriteLine();
                        Console.WriteLine("Niepoprawnie wporwadzono słowa, spróbuj jeszcze raz :");
                        Console.WriteLine();
                        bl = false;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return bl;
        }
    }
}
