﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Bank
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //        KLASA BAZA DANYCH :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    [Serializable]
    public class DataBase
    {
        // POLA KALSY BAZA DANYCH :

        private List<Int32> userNumbersList;
        private List<Int64> accountNumbersList;
        private List<User> usersList;

        // KONTRUKTORY KLASY BAZA DANYCH :

        public DataBase()
        {
        }

        public DataBase(List<Int32> userNumbersList, List<Int64> accountNumbersList, List<User> usersList)
        {
            this.userNumbersList = userNumbersList;
            this.accountNumbersList = accountNumbersList;
            this.usersList = usersList;
        }

        // METODY ZWRACAJĄCE WARTOŚCI PÓL KLASY :

        public static List<Int32> GetUserNumbersList(DataBase db)
        {
            return db.userNumbersList;
        }

        public static List<Int64> GetAccountNumbersList(DataBase db)
        {
            return db.accountNumbersList;
        }

        public static List<User> GetUsersList(DataBase db)
        {
            return db.usersList;
        }

        // METODA ZAPISU DO PLIKU (SERIALIZACJA) :

        public static void Serialize(DataBase database)
        {
            FileStream save1 = new FileStream("UserNumbersList.txt", FileMode.OpenOrCreate);
            FileStream save2 = new FileStream("AccountNumbersList.txt", FileMode.OpenOrCreate);
            FileStream save3 = new FileStream("UsersList.txt", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(save1, database.userNumbersList);
            formatter.Serialize(save2, database.accountNumbersList);
            formatter.Serialize(save3, database.usersList);

            save1.Close();
            save2.Close();
            save3.Close();
        }

        // METODA ODCZYTU DANYCH Z PLIKU (DESERIALIZACJA) :

        public static DataBase Deserialize()
        {
            DataBase database;
            try
            {
                FileStream load1 = new FileStream("UserNumbersList.txt", FileMode.Open);
                FileStream load2 = new FileStream("AccountNumbersList.txt", FileMode.Open);
                FileStream load3 = new FileStream("UsersList.txt", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();

                List<Int32> userNumbersList = (List<Int32>)formatter.Deserialize(load1);
                List<Int64> accountNumbersList = (List<Int64>)formatter.Deserialize(load2);
                List<User> usersList = (List<User>)formatter.Deserialize(load3);

                database = new DataBase(userNumbersList, accountNumbersList, usersList);

                load1.Close();
                load2.Close();
                load3.Close();
            }
            catch
            {
                database = new DataBase(new List<Int32>(), new List<Int64>(), new List<User>());
            }       
            return database;
        }
    }
}
