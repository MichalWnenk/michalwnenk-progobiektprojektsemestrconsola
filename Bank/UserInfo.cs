﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //      KLASA INFO UŻYTKOWNIKA :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    [Serializable]
    public class UserInfo
    {
        // POLA KLASY INFO UŻYTKOWNIKA :

        private String name;
        private String surname;
        private String birthDate;
        private Int64 pesel;
        private String addres;
        private String postCode;

        // KONSTRUKTORY KLASY UŻYTKOWNIK :

        public UserInfo()
        {
        }

        public UserInfo(String name, String surname, String birthDate, Int64 pesel, String addres, String postCode)
        {
            this.name = name;
            this.surname = surname;
            this.birthDate = birthDate;
            this.pesel = pesel;
            this.addres = addres;
            this.postCode = postCode;
        }

        // METODY ZWRACAJĄCE WARTOŚCI PÓL KLASY :

        public static string GetName(UserInfo ui)
        {
            return ui.name;
        }

        public static string GetSurname(UserInfo ui)
        {
            return ui.surname;
        }

        // METODA REJESTRACJI :

        public static void Registration(UserInfo ui)
        {
            Console.WriteLine("Witaj! W celu rejestracji wypełnij poniższe pola.");
            Console.WriteLine();

            // SPRAWDZA CZY PODAŁEŚ SŁOWO :
            string name;
            string surname;
            Boolean bl = true;
            do
            {
                Console.WriteLine("Imię : ");
                name = Console.ReadLine();
                Console.WriteLine();

                Console.WriteLine("Nazwisko : ");
                surname = Console.ReadLine();
                Console.WriteLine();

                if (CheckWord(name) && CheckWord(surname))
                {
                    bl = true;
                }
                else
                {
                    bl = false;
                }
            } while (!bl);
            ui.name = name;
            ui.surname = surname;

            Console.WriteLine("Datę urodzenia : ");

            Console.WriteLine("Dzień :");
            string dzien = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Miesiąc :");
            string miesiac = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Rok :");
            string rok = Console.ReadLine();
            Console.WriteLine();

            ui.birthDate = dzien + "-" + miesiac + "-" + rok;

            Int64 pesel = 0;
            do
            {
                try
                {
                    Console.WriteLine("Pesel :");
                    pesel = Convert.ToInt64(Console.ReadLine());
                    Console.WriteLine();
                    bl = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Nie wprowadziłeś poprawnie liczby!");
                    Console.WriteLine();
                    bl = false;
                }

            } while (!bl);
            ui.pesel = pesel;

            Console.WriteLine("Adres (ulica, numer bloku, numer mieszkania) :");
            ui.addres = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Kod pocztowy (z miejscowością) :");
            ui.postCode = Console.ReadLine();
            Console.WriteLine();
        }

        // METODA SPRAWDZAJĄCA CZY PODANY CIĄG ZNAKÓW TO SŁOWO :

        public static Boolean CheckWord(string word)
        {
            Boolean bl = true;
            foreach (var letter in word)
            {
                while (bl == true)
                {
                    if (!(Char.IsLetter(letter) || letter == ' '))
                    {
                        Console.WriteLine("Niepoprawnie wporwadzono słowa, spróbuj jeszcze raz :");
                        Console.WriteLine();
                        bl = false;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return bl;
        }

        // METODA POKAZUJĄCA INFORMACJE O UŻYTKOWNIKU :

        public static void ShowUserInfo(UserInfo userInfo)
        {
            Boolean bl = true;
            do
            {
                Console.WriteLine("Imię : {0}", userInfo.name);
                Console.WriteLine();
                Console.WriteLine("Nazwisko : {0}", userInfo.surname);
                Console.WriteLine();
                Console.WriteLine("Datę urodzenia : {0}", userInfo.birthDate);
                Console.WriteLine();
                Console.WriteLine("Pesel : {0}", userInfo.pesel);
                Console.WriteLine();
                Console.WriteLine("Adres : {0}", userInfo.addres);
                Console.WriteLine();
                Console.WriteLine("Kod pocztowy : {0}", userInfo.postCode);
                Console.WriteLine();
                Console.WriteLine("1 -> Wprowadż zmiany");
                Console.WriteLine("2 -> Powrót");
                int number = Convert.ToInt32(Console.ReadLine());

                if (number == 1 || number == 2)
                {
                    if (number == 1)
                    {
                        Registration(userInfo);
                        bl = false;
                    }
                    else
                    {
                        bl = false;
                    }
                }
                else
                {
                    Console.WriteLine("Nieprawidłowy numer metody. Sprobuj jeszcze raz :");
                }
            } while (bl);
        }
    }
}
