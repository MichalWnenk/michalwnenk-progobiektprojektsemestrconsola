﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank; // bibliotek DLL "Bank"

namespace ProgObiektProjektSemestrConsola
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //       INTERFACE MENUPANEL :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    interface IMenuPanel
    {
        // MENU GŁOWNE :
        void MainMenu(List<User> usersList, List<Int32> userNumbersList, List<Int64> accountNumbersList);

        // PANEL LOGOWANIE :
        void Login(List<User> usersList, List<Int32> userNumbersList, List<Int64> accountNumbersList);

        // MENU UŻYTKOWNIKA :
        void UserMenu(User user, List<Int64> accountNumbersList, List<User> usersList, List<Int32> userNumbersList);

        // MENU "TWORZENIE NOWEGO KONTA" :
        void OpenAccountMenu(User user, List<Int64> accountNumbersList, List<User> usersList, List<Int32> userNumbersList);

        // MENU WYŚWIETLANIA HISTORII PRZELEWÓW :
        void HistoryTransferListMenu(User user);
    }
}
