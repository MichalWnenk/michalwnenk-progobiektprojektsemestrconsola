﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank; // bibliotek DLL "Bank"

namespace ProgObiektProjektSemestrConsola
{
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|
    //            KLASA MENU :
    // |>>>>>>>>>>>>>>>|<<<<<<<<<<<<<<<|

    class MenuPanel : IMenuPanel
    {
        // MENU GŁOWNE :

        public void MainMenu(List<User> usersList, List<Int32> userNumbersList, List<Int64> accountNumbersList)
        {
            MenuPanel mp = new MenuPanel();

            Console.WriteLine(">> MENU GŁÓWNE <<");
            Console.WriteLine("1 -> Logowanie");
            Console.WriteLine("2 -> Rejestracja");
            Console.WriteLine("3 -> Wyjdź z banku");

            int number = Convert.ToInt32(Console.ReadLine());

            if (number == 1 || number == 2 || number == 3)
            {
                switch (number)
                {
                    // LOGOWANIE :
                    case 1:
                        mp.Login(usersList, userNumbersList, accountNumbersList);
                        break;

                    // REJETRACJA :
                    case 2:
                        User user = new User(0, "", new UserInfo(), new List<Account>());
                        user.CreateUserLogin(user, userNumbersList);
                        userNumbersList.Add(User.GetUserNumber(user));
                        usersList.Add(user);
                        DataBase database = new DataBase(userNumbersList, accountNumbersList, usersList);
                        DataBase.Serialize(database);
                        mp.MainMenu(usersList, userNumbersList, accountNumbersList);
                        break;
                    case 3:
                        break;
                }
            }
            else
            {
                Console.WriteLine("Zły numer operacji. Spróbuj jeszcze raz :");
                Console.WriteLine();
                mp.MainMenu(usersList, userNumbersList, accountNumbersList);
            }
        }

        // PANEL LOGOWANIE :

        public void Login(List<User> usersList, List<Int32> userNumbersList, List<Int64> accountNumbersList)
        {
            MenuPanel mp = new MenuPanel();

            Console.WriteLine("Podaj numer użytkownika :");
            int userNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj hasło :");
            string password = Console.ReadLine();
            Boolean bl = true;

            if (usersList != null)
            {
                foreach (var item in usersList)
                {
                    if (User.GetUserNumber(item) == userNumber && User.GetPassword(item) == password)
                    {
                        bl = false;
                        Console.WriteLine();
                        mp.UserMenu(item, accountNumbersList, usersList, userNumbersList);
                        break;
                    }
                }

                if (bl)
                {
                    Console.WriteLine();
                    Console.WriteLine("Podałeś nieistniejący numer użytkownika lub nieprawidłowe hasło");
                    Console.WriteLine();
                    mp.MainMenu(usersList, userNumbersList, accountNumbersList);
                }

            }
        }

        // MENU UŻYTKOWNIKA :

        public void UserMenu(User user, List<Int64> accountNumbersList, List<User> usersList, List<Int32> userNumbersList)
        {
            MenuPanel mp = new MenuPanel();

            Console.WriteLine(">> Zalogowany jako {0} {1} <<", UserInfo.GetName(User.GetUserInfo(user)), UserInfo.GetSurname(User.GetUserInfo(user)));
            Console.WriteLine();
            Console.WriteLine("1 -> Pokaż moje rachunki");
            Console.WriteLine("2 -> Otwórz nowy rachunek");
            Console.WriteLine("3 -> Usuń rachunek");
            Console.WriteLine("4 -> Wykonaj przelew");
            Console.WriteLine("5 -> Zobacz historię przelewów");
            Console.WriteLine("6 -> Moje dane");
            Console.WriteLine("7 -> Wyloguj");
            Console.WriteLine("0 -> Usuń moje konto użytkownika");
            int number = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            switch (number)
            {
                // POKAŻ MOJE RACHUNKI :

                case 1:
                    int liczba = 1;
                    Console.WriteLine("Twoje rachunki :");
                    Console.WriteLine();
                    if (User.GetAccountsList(user) != null)
                    {
                        foreach (var item in User.GetAccountsList(user))
                        {
                            Console.WriteLine("---------------<{0}>-------------", liczba);
                            item.ShowAccountInfo();
                            Console.WriteLine("-------------------------------");
                            Console.WriteLine();
                            liczba += 1;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nie utworzyłeś jeszcze ani jednego konto :)");
                        Console.WriteLine();
                    }
                    UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;

                // OTWÓRZ NOWY RACHUNEK :

                case 2:
                    mp.OpenAccountMenu(user, accountNumbersList, usersList, userNumbersList);
                    DataBase database1 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database1);
                    Console.WriteLine();
                    UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;

                // USUŃ RACHUNEK :

                case 3:
                    Console.WriteLine("Podaj numer konta, które chcesz usunąć ?");
                    Console.WriteLine();
                    int number1 = 1;
                    if (User.GetAccountsList(user) != null)
                    {
                        foreach (var item in User.GetAccountsList(user))
                        {
                            Console.WriteLine("{0}. {1} [ {2} ]", number1, Account.GetType(item), Account.GetAccountNumber(item));
                            number1 += 1;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nie utworzyłeś jeszcze ani jednego konto :)");
                        Console.WriteLine();
                    }

                    int number2 = Convert.ToInt32(Console.ReadLine());
                    User.GetAccountsList(user).Remove(User.GetAccountsList(user)[number2 - 1]);
                    Console.WriteLine();
                    Console.WriteLine("Wybrane przez Ciebie konto rachunkowe zostało pomyślnie usunięte :)");
                    Console.WriteLine();
                    DataBase database2 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database2);
                    Console.WriteLine();
                    UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;

                // WYKONAJ PRZELEWE :

                case 4:
                    Transfer.MakeTransfer(user, usersList);
                    DataBase database3 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database3);
                    Console.WriteLine();
                    UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;

                // ZOBACZ HISTORIĘ PRZELEWÓW :

                case 5:
                    mp.HistoryTransferListMenu(user);
                    DataBase database4 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database4);
                    Console.WriteLine();
                    mp.UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;
                case 6:
                    UserInfo.ShowUserInfo(User.GetUserInfo(user));
                    DataBase database5 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database5);
                    Console.WriteLine();
                    mp.UserMenu(user, accountNumbersList, usersList, userNumbersList);
                    break;

                // WYLOGUJ :

                case 7:
                    DataBase database6 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database6);
                    mp.MainMenu(usersList, userNumbersList, accountNumbersList);
                    break;

                // USUŃ MOJE KONTO UŻYTKOWNIKA :

                case 0:
                    usersList.Remove(user);
                    Console.WriteLine("Twoje konto użytkownika zostało pomyślnie usunięte :)");
                    DataBase database0 = new DataBase(userNumbersList, accountNumbersList, usersList);
                    DataBase.Serialize(database0);
                    mp.MainMenu(usersList, userNumbersList, accountNumbersList);
                    break;
            }
        }

        // MENU "TWORZENIE NOWEGO KONTA" :

        public void OpenAccountMenu(User user, List<Int64> accountNumbersList, List<User> usersList, List<Int32> userNumbersList)
        {
            MenuPanel mp = new MenuPanel();

            Console.WriteLine(" >> OTWIERANIE NOWEGO RACHUNKU <<");
            Console.WriteLine();
            Console.WriteLine("Jaki rachunek chcesz otworzyć ?");
            Console.WriteLine("1 -> Normalny");
            Console.WriteLine("2 -> Junior");
            Console.WriteLine("3 -> Profit");
            int number = Convert.ToInt32(Console.ReadLine());

            switch (number)
            {
                // OTWIERANIE KONTA NORAMLNEGO :
                case 1:
                    Account.NormalAccount naccount = new Account.NormalAccount("Konto Normalne", Account.OpenAccount(accountNumbersList), 0, 0, new List<Transfer>());
                    User.GetAccountsList(user).Add(naccount);
                    Console.WriteLine();
                    Console.WriteLine("Rachunek 'Konto Normalne' został pomyślenie otwarty :)");
                    Console.WriteLine();
                    break;

                // OTWIERANIE KONTA JUNIOR :
                case 2:
                    Account.JuniorAccount jaccount = new Account.JuniorAccount("Konto Junior", Account.OpenAccount(accountNumbersList), 0, 0, Account.JuniorAccount.SetAgent(), new List<Transfer>());
                    User.GetAccountsList(user).Add(jaccount);
                    Console.WriteLine();
                    Console.WriteLine("Rachunek 'Konto Junior' został pomyślenie otwarty :)");
                    Console.WriteLine();
                    break;

                // OTWIERANIE KONTA PROFIT :
                case 3:
                    Account.ProfitAccount paccount = new Account.ProfitAccount("Konto Profit", Account.OpenAccount(accountNumbersList), 0, 0, new List<Transfer>());
                    User.GetAccountsList(user).Add(paccount);
                    Console.WriteLine();
                    Console.WriteLine("Rachunek 'Konto Profit' został pomyślenie otwarty :)");
                    Console.WriteLine();
                    break;
            }
        }

        // MENU WYŚWIETLANIA HISTORII PRZELEWÓW :

        public void HistoryTransferListMenu(User user)
        {
            MenuPanel mp = new MenuPanel();

            if (User.GetAccountsList(user) != null)
            {
                Console.WriteLine("Wybierz numer konta, którego historie chcesz zobaczyć :");
                Console.WriteLine();
                int number = 1; // pomocnicza zmienna do wypisywania kont
                foreach (var item in User.GetAccountsList(user))
                {
                    Console.WriteLine("{0} -> {1}, [ {2} ]", number, Account.GetType(item), Account.GetAccountNumber(item));
                    Console.WriteLine();
                    number += 1;
                }
                Console.WriteLine("0 -> Powrót");

                int number1 = Convert.ToInt32(Console.ReadLine()); // numer konto wybrany przez użytkownika

                if (number1 != 0)
                {
                    if (number1 > 0 && number1 < number)
                    {
                        Console.WriteLine();
                        Transfer.ShowTransferInfo(Account.GetTransferHistoryList(User.GetAccountsList(user)[number1 - 1]));
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Podałeś numer nieistniejącego konta. Spróbuj jeszcze raz :");
                        HistoryTransferListMenu(user);
                    }
                }
            }
            else
            {
                Console.WriteLine("Nie wykonałeś jeszcze ani jednego przelewu :)");
                Console.WriteLine();
            }
        }
    }
}
