﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank; // bibliotek DLL "Bank"

namespace ProgObiektProjektSemestrConsola
{
    class Program
    {
        // METODA GŁOWNA PROGRAMU : 

        static void Main(string[] args)
        {
            DataBase database = DataBase.Deserialize();
            List<Int32> userNumbersList = DataBase.GetUserNumbersList(database);
            List<Int64> accountNumbersList = DataBase.GetAccountNumbersList(database);
            List<User> usersList = DataBase.GetUsersList(database);

            // DANE TESTOWE JUŻ WPROWADZONE (NIEODKOMENTOWYWAĆ!) :

            //User Admin = new User(77777777, "12345678", new UserInfo("Admin", "Admin", "0-0-0", 0, "Admin", "Admin"),
            //    new List<Account> { new Account.NormalAccount("Konto Normalne(Admin)", 0, 654632947, 654632947, 
            //        new List<Transfer> { new Transfer(7586257122974519056, "Elvis", "Presley", "Księżyc 45b", "00-000", -1265465),
            //                             new Transfer(5331758915638964813, "Marta", "Ważna", "Polna 43", "02-987", 347691),
            //                             new Transfer(7135973519745239876, "Lena", "Makowska", "Rakowa 1", "02-412", -871546) }) });

            //User user1 = new User(73546490, "12345678", new UserInfo("Elvis", "Presley", "8-1-1935", 35010842941, "Księżyc 45b", "00-000"),
            //    new List<Account> { new Account.NormalAccount("Konto Normalne", 7586257122974519056, 87457676, 87457676, 
            //        new List<Transfer> { new Transfer(0, "Admin", "Admin", "Admin", "Admin", 1265465),
            //                             new Transfer(5331758915638964813, "Marta", "Ważna", "Polna 43", "02-987", -83265),
            //                             new Transfer(7135973519745239876, "Lena", "Makowska", "Rakowa 1", "02-412", 9862) }) });

            //User user2 = new User(58038756, "12345678", new UserInfo("Marta", "Ważna", "23-7-1980", 80072351964, "Polna 43", "02-987"),
            //    new List<Account> { new Account.JuniorAccount("Konto Junior", 5331758915638964813, 746332, 746332, "Maria Ważna",
            //        new List<Transfer> { new Transfer(0, "Admin", "Admin", "Admin", "Admin", -347691),
            //                             new Transfer(7586257122974519056, "Elvis", "Presley", "Księżyc 45b", "00-000", 83265),
            //                             new Transfer(7135973519745239876, "Lena", "Makowska", "Rakowa 1", "02-412", -1543) }) });

            //User user3 = new User(26582644, "12345678", new UserInfo("Lena", "Makowska", "5-11-1991", 91110542908, "Rakowa 1", "02-412"),
            //    new List<Account> { new Account.ProfitAccount("Konto Profit", 7135973519745239876, 19895554, 19895554, 
            //        new List<Transfer> { new Transfer(0, "Admin", "Admin", "Admin", "Admin", 871546),
            //                             new Transfer(7586257122974519056, "Elvis", "Presley", "Księżyc 45b", "00-000", -9862),
            //                             new Transfer(5331758915638964813, "Marta", "Ważna", "Polna 43", "02-987", 1543) }) });

            //usersList.Add(Admin);
            //userNumbersList.Add(User.GetUserNumber(Admin));
            //accountNumbersList.Add(Account.GetAccountNumber(User.GetAccountsList(Admin)[0]));
            //usersList.Add(user1);
            //userNumbersList.Add(User.GetUserNumber(user1));
            //accountNumbersList.Add(Account.GetAccountNumber(User.GetAccountsList(user1)[0]));
            //usersList.Add(user2);
            //userNumbersList.Add(User.GetUserNumber(user2));
            //accountNumbersList.Add(Account.GetAccountNumber(User.GetAccountsList(user2)[0]));
            //usersList.Add(user3);
            //userNumbersList.Add(User.GetUserNumber(user3));
            //accountNumbersList.Add(Account.GetAccountNumber(User.GetAccountsList(user3)[0]));

            // POWITANIE NA STARCIE :

            Console.WriteLine(">> ------------------------------------- <<");
            Console.WriteLine("<< WITAJ W BANKU INTERNETOWYM [M&W Bank] >>");
            Console.WriteLine(">> ------------------------------------- <<");
            Console.WriteLine();

            MenuPanel mp = new MenuPanel();

            mp.MainMenu(usersList, userNumbersList, accountNumbersList);
        }
    }
}
